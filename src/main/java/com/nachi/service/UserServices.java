package com.nachi.service;

import com.nachi.dao.UserDao;
import com.nachi.model.DAOUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServices {
 
    public static final int MAX_FAILED_ATTEMPTS = 3;
     
    private static final long LOCK_TIME_DURATION = 24 * 60 * 60 * 1000; // 24 hours
     
    @Autowired
    private UserDao repo;

    public DAOUser getUserDetails(String userName){
        return repo.findByUsername(userName);
    };

    public DAOUser saveOrUpdateUser(DAOUser user){
        return repo.save(user);
    }
     
    public void increaseFailedAttempts(DAOUser user) {
        int newFailAttempts = user.getFailedAttempt() + 1;
        repo.updateFailedAttempts(newFailAttempts, user.getUsername());
    }
     
    public void resetFailedAttempts(String email) {
        repo.updateFailedAttempts(0, email);
    }
     
    public void lock(DAOUser user) {
        user.setAccountNonLocked(false);
        repo.save(user);
    }

}