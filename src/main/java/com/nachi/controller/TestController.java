package com.nachi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@RequestMapping({ "/test" })
	public String firstPage() {
		return "Working...";
	}

	@GetMapping("/api/qparm")
	@ResponseBody
	public String testXss(@RequestParam String id) {
		System.out.println("id :::: "+id);
		return "ID: " + id;
	}
}