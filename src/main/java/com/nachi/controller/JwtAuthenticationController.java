package com.nachi.controller;

import com.nachi.model.DAOUser;
import com.nachi.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import  org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import com.nachi.service.JwtUserDetailsService;


import com.nachi.config.JwtTokenUtil;
import com.nachi.model.JwtRequest;
import com.nachi.model.JwtResponse;
import com.nachi.model.UserDTO;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Autowired
	private UserServices userServices;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception  {

	DAOUser user = userServices.getUserDetails(authenticationRequest.getUsername());
    if (user.isAccountNonLocked()){
    	    try {
    	    	if(user.getFailedAttempt()>0) {
			authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword(), user);
			userServices.resetFailedAttempts(user.getUsername());
    	    	}else {
    	    		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword(), user);
    	    	}
    	    }catch (DisabledException e) {
    			throw new Exception("USER_DISABLED", e);
    		} catch (BadCredentialsException e) {
    			saveWhenFailedLogin(user);
    			throw new Exception("INVALID_CREDENTIALS", e);
    		}finally {
    			if(user.getFailedAttempt()==2) {
    				HttpHeaders responseHeaders = new HttpHeaders();
    				responseHeaders.set("error", "going to get locked one more attempt remaining...");
    				return new ResponseEntity<String>("One more attempt remaining", responseHeaders, HttpStatus.UNAUTHORIZED);
    			}else if(user.getFailedAttempt()==3) {
    				return lockAccount();
    			}
    		}
	}else{
		return lockAccount();
	}
    
	final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);
		
		return ResponseEntity.ok(new JwtResponse(token,authenticationRequest.getUsername()));
	}
	
	  final ResponseEntity<String> lockAccount(){
	    	HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.set("error", "locked");
			return new ResponseEntity<String>("Account is locked", responseHeaders, HttpStatus.LOCKED);
	    }
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> saveUser(@RequestBody UserDTO user) throws Exception {
		return ResponseEntity.ok(userDetailsService.save(user));
	}

	private void authenticate(String username, String password,DAOUser user) throws DisabledException,BadCredentialsException {
				authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
	}
	
	public void saveWhenFailedLogin(DAOUser user) {
		int failedAttempt = user.getFailedAttempt();
		failedAttempt+=1;
		user.setFailedAttempt(failedAttempt);
		if(failedAttempt>=3) {
		user.setAccountNonLocked(false);
		}
		user = userServices.saveOrUpdateUser(user);
		
	}


}