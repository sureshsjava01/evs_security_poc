package com.nachi.model;

import java.io.Serializable;

public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;
	private final String jwttoken;
	private final String jwtUserName;

	public JwtResponse(String jwttoken,String jwtUserName) {
		this.jwttoken = jwttoken;
		this.jwtUserName = jwtUserName;
	}

	public String getToken() {
		return this.jwttoken;
	}

	@Override
	public String toString() {
		return "JwtResponse{" +
				"jwttoken='" + jwttoken + '\'' +
				", jwtUserName='" + jwtUserName + '\'' +
				'}';
	}
}