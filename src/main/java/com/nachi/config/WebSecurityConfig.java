package com.nachi.config;

import com.nachi.model.DAOUser;
import com.nachi.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import java.security.SecureRandom;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	@Autowired
	private UserDetailsService jwtUserDetailsService;

	@Autowired
	private JwtRequestFilter jwtRequestFilter;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		// configure AuthenticationManager so that it knows from where to load
		// user for matching credentials
		// Use BCryptPasswordEncoder
		auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		SecureRandom secureRandom = new SecureRandom();
		secureRandom.setSeed(new byte[100]);
		return new BCryptPasswordEncoder(10, secureRandom);
	}


	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}


	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		// We don't need CSRF for this example
		
		httpSecurity.csrf().disable()
				// dont authenticate this particular request
				.authorizeRequests().antMatchers("/authenticate", "/register").permitAll().
				// all other requests need to be authenticated
				anyRequest().authenticated().and().
				// make sure we use stateless session; session won't be used to
				// store user's state.
				exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		/*
		 * httpSecurity .exceptionHandling() .authenticationEntryPoint( (request,
		 * response, ex) -> {
		 * 
		 * // We should write our logic hear I guess
		 * if(ex.getMessage().equals("Bad credentials")){
		 * 
		 * //Object principle =
		 * SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		 * //principle. //SecurityContext securityContext =
		 * SecurityContextHolder.getContext(); //Object obj =
		 * securityContext.getAuthentication().getName().toString();
		 * //System.out.println("+++++ ::: "+securityContext.getAuthentication());
		 * 36Authentication authentication =
		 * SecurityContextHolder.getContext().getAuthentication(); if (!(authentication
		 * instanceof AnonymousAuthenticationToken)) {
		 * 
		 * 
		 * } if (obj instanceof DAOUser) { DAOUser springSecurityUser = (DAOUser) obj;
		 * String userConnectedName = springSecurityUser.getUsername();
		 * System.out.println("userConnectedName :: "+userConnectedName); }
		 * //System.out.println("User Name :: "+request.getParameter("username"));
		 * //userService.increaseFailedAttempts(); }
		 * 
		 * response.sendError( HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage() ); }
		 * ) .and();
		 */

		/*
		 * httpSecurity .formLogin() .loginProcessingUrl("/loginForm")
		 * .failureHandler(new CustomAuthenticationFailureHandler());
		 */

		// Add a filter to validate the tokens with every request
		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
		httpSecurity.addFilterBefore(new XSSFilter(), BasicAuthenticationFilter.class);
	}

}